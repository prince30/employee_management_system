Employee Management System Rest API


**Create Department**

End Point- http://localhost:9000/employee-management-system/api/v1/departments
Method -POST
{
"name":"new6",
"active":1
}

**Update Department**

End Point- http://localhost:9000/employee-management-system/api/v1/departments/1
Method -PUT
{
"name":"test",
"active":1
}

**Get Specific Department**

End Point- http://localhost:9000/employee-management-system/api/v1/departments/1

Method -GET

**Get All Departments**

End Point- http://localhost:9000/employee-management-system/api/v1/departments
Method -GET

**Get Specific Delete Department**

End Point- http://localhost:9000/employee-management-system/api/v1/departments/1

**Create Employee**

End Point- http://localhost:9000/employee-management-system/api/v1/departments
Method -POST
{
"name":"test",
"dateOfBirth": "2021-09-11",
"mobile": "01700000000",
"gender": "MALE",
"departmentId": 4
}

**Update Employee**

End Point- http://localhost:9000/employee-management-system/api/v1/departments/1
Method -PUT
{
"name":"test",
"dateOfBirth": "2021-09-11",
"mobile": "01700000000",
"gender": "MALE",
"departmentId": 3
}

**Get Specific Employee**

End Point- http://localhost:9000/employee-management-system/api/v1/departments/1

Method -GET

**Get All Employees**

End Point- http://localhost:9000/employee-management-system/api/v1/departments
Method -GET

**Get Specific Delete Employee**

End Point- http://localhost:9000/employee-management-system/api/v1/departments/1
Method -DELETE

