package employee.management.system.rest.api.request;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class DepartmentRequestData {

	@NotEmpty(message="Name field is required")
	private String name;
	@NotNull(message="Please select check box")
	private Boolean active;
	private List<EmployeeRequestData> employees;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<EmployeeRequestData> getEmployees() {
		return employees;
	}

	public void setEmployees(List<EmployeeRequestData> employees) {
		this.employees = employees;
	}

}
