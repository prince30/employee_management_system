package employee.management.system.rest.api.service;

import java.util.List;
import employee.management.system.rest.api.dto.EmployeeDto;

public interface EmployeeService {

	EmployeeDto create(EmployeeDto employeeDto);

	EmployeeDto update(Long employeeId, EmployeeDto employeeDto);
	
	EmployeeDto fetchById(Long employeeId);

	void delete(Long employeeId);
	
	List<EmployeeDto> fetchAll(int page, int limit);

	List<EmployeeDto> fetchEmployeesByDepartmentId(Long departmentId, int page, int limit);
}
