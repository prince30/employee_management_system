package employee.management.system.rest.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.management.RuntimeErrorException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import employee.management.system.rest.api.dto.DepartmentDto;
import employee.management.system.rest.api.entity.Department;
import employee.management.system.rest.api.repository.DepartmentRepository;
import employee.management.system.rest.api.service.DepartmentService;

@Service
public class DepartmentServiceImpl implements DepartmentService {

	private DepartmentRepository departmentRepository;

	public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
		this.departmentRepository = departmentRepository;
	}

	@Override
	public DepartmentDto create(DepartmentDto departmentDto) {
		ModelMapper modelMapper = new ModelMapper();
		Department departmentEntity = modelMapper.map(departmentDto, Department.class);

		departmentEntity.setName(departmentDto.getName());
		departmentEntity.setActive(departmentDto.getActive());

		Department departmentStore = departmentRepository.save(departmentEntity);
		DepartmentDto returnValue = modelMapper.map(departmentStore, DepartmentDto.class);

		return returnValue;
	}

	@Override
	public DepartmentDto fetchById(Long departmentId) {
		DepartmentDto returnValue = new DepartmentDto();

		Department departmentEntity = departmentRepository.findById(departmentId).get();

		if (departmentEntity == null) {
			throw new RuntimeErrorException(null, "ID is not found");
		}
		BeanUtils.copyProperties(departmentEntity, returnValue);

		return returnValue;
	}

	@Override
	public DepartmentDto update(Long departmentId, DepartmentDto departmentDto) {
		DepartmentDto returnValue = new DepartmentDto();

		Department departmentEntity = departmentRepository.findById(departmentId).get();

		departmentEntity.setName(departmentDto.getName());
		departmentEntity.setActive(departmentDto.getActive());

		Department updateDepartment = departmentRepository.save(departmentEntity);

		BeanUtils.copyProperties(updateDepartment, returnValue);

		return returnValue;
	}

	@Override
	public void delete(Long departmentId) {
		Department departmentEntity = departmentRepository.findById(departmentId).get();

		departmentRepository.delete(departmentEntity);
	}

	@Override
	public List<DepartmentDto> fetchAll(int page, int limit) {
		List<DepartmentDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Page<Department> departmentPage = departmentRepository.findAll(pageableRequest);
		List<Department> departments = departmentPage.getContent();

		for (Department department : departments) {
			DepartmentDto departmentDto = new DepartmentDto();

			BeanUtils.copyProperties(department, departmentDto);
			returnValue.add(departmentDto);
		}

		return returnValue;
	}

}
