package employee.management.system.rest.api.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import employee.management.system.rest.api.dto.DepartmentDto;
import employee.management.system.rest.api.dto.EmployeeDto;
import employee.management.system.rest.api.request.DepartmentRequestData;
import employee.management.system.rest.api.response.DepartmentResponse;
import employee.management.system.rest.api.response.EmployeeResponse;
import employee.management.system.rest.api.response.ErrorMessages;
import employee.management.system.rest.api.response.OperationStatusModel;
import employee.management.system.rest.api.response.RequestOperationStatus;
import employee.management.system.rest.api.service.DepartmentService;
import employee.management.system.rest.api.service.EmployeeService;

@RestController
@RequestMapping("departments") // http://localhost:9000/employee-management-system/api/v1/departments
public class DepartmentController {

	private DepartmentService departmentService;
	private EmployeeService employeeService;

	public DepartmentController(DepartmentService departmentService, EmployeeService employeeService) {
		this.departmentService = departmentService;
		this.employeeService = employeeService;
	}

	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<DepartmentResponse> store(@Valid @RequestBody DepartmentRequestData departmentRequestData)
			throws Exception {

		DepartmentResponse returnValue = new DepartmentResponse();

		ModelMapper modelMapper = new ModelMapper();
		DepartmentDto departmentDto = modelMapper.map(departmentRequestData, DepartmentDto.class);

		DepartmentDto createdDepartment = departmentService.create(departmentDto);

		returnValue = modelMapper.map(createdDepartment, DepartmentResponse.class);

		return new ResponseEntity<DepartmentResponse>(returnValue, HttpStatus.CREATED);
	}

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<DepartmentResponse> fetchById(@PathVariable Long id) throws Exception {

		DepartmentResponse returnValue = new DepartmentResponse();

		DepartmentDto departmentDto = departmentService.fetchById(id);

		BeanUtils.copyProperties(departmentDto, returnValue);

		return new ResponseEntity<DepartmentResponse>(returnValue, HttpStatus.FOUND);
	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<DepartmentResponse> update(@Valid @PathVariable Long id,
			@RequestBody DepartmentRequestData departmentRequestData) throws Exception {
		if (departmentRequestData.getName().isEmpty() || departmentRequestData.getActive() == null)
			throw new Exception(ErrorMessages.MISSING_REQUIRED_FIELD.getErrorMessage());

		DepartmentResponse returnValue = new DepartmentResponse();

		DepartmentDto departmentDto = new DepartmentDto();

		BeanUtils.copyProperties(departmentRequestData, departmentDto);

		DepartmentDto updateDepartment = departmentService.update(id, departmentDto);

		BeanUtils.copyProperties(updateDepartment, returnValue);

		return new ResponseEntity<DepartmentResponse>(returnValue, HttpStatus.OK);
	}

	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<OperationStatusModel> delete(@PathVariable Long id) throws Exception {

		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		departmentService.delete(id);
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());

		return new ResponseEntity<OperationStatusModel>(returnValue, HttpStatus.OK);
	}

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<DepartmentResponse>> fetchAll(
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "5") int limit) {

		List<DepartmentResponse> returnValue = new ArrayList<>();

		List<DepartmentDto> departments = departmentService.fetchAll(page, limit);

		for (DepartmentDto departmentDto : departments) {
			DepartmentResponse departmentResponseModel = new DepartmentResponse();
			BeanUtils.copyProperties(departmentDto, departmentResponseModel);
			returnValue.add(departmentResponseModel);
		}

		return new ResponseEntity<List<DepartmentResponse>>(returnValue, HttpStatus.FOUND);
	}

	@GetMapping(path = "/{id}/employees", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<EmployeeResponse>> fetchEmployeesByDepartmentId(@PathVariable Long id,
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "5") int limit) {

		List<EmployeeResponse> returnValue = new ArrayList<>();

		List<EmployeeDto> employeesDto = employeeService.fetchEmployeesByDepartmentId(id, page, limit);

		if (employeesDto != null && !employeesDto.isEmpty()) {
			java.lang.reflect.Type listType = new TypeToken<List<EmployeeResponse>>() {
			}.getType();
			returnValue = new ModelMapper().map(employeesDto, listType);
		}

		return new ResponseEntity<List<EmployeeResponse>>(returnValue, HttpStatus.FOUND);
	}
}
