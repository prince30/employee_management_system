package employee.management.system.rest.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import employee.management.system.rest.api.entity.Department;

@Repository
public interface DepartmentRepository extends PagingAndSortingRepository<Department, Long> {

}
