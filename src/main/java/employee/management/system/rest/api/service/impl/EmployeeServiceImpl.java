package employee.management.system.rest.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.management.RuntimeErrorException;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import employee.management.system.rest.api.dto.EmployeeDto;
import employee.management.system.rest.api.entity.Department;
import employee.management.system.rest.api.entity.Employee;
import employee.management.system.rest.api.repository.DepartmentRepository;
import employee.management.system.rest.api.repository.EmployeeRepository;
import employee.management.system.rest.api.service.EmployeeService;
import employee.management.system.rest.api.util.UniqueCode;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeRepository employeeRepository;
	private DepartmentRepository departmentRepository;
	private UniqueCode uniqueCode;

	public EmployeeServiceImpl(EmployeeRepository employeeRepository, UniqueCode uniqueCode,
			DepartmentRepository departmentRepository) {
		this.employeeRepository = employeeRepository;
		this.uniqueCode = uniqueCode;
		this.departmentRepository = departmentRepository;
	}

	@Override
	public EmployeeDto create(EmployeeDto employeeDto) {
		Employee employeeEntity = new Employee();
		BeanUtils.copyProperties(employeeDto, employeeEntity);

		Department departmentEntity = departmentRepository.findById(employeeDto.getDepartmentId()).get();

		String code = uniqueCode.generateCode(4);

		employeeEntity.setCode(code);
		employeeEntity.setName(employeeDto.getName());
		employeeEntity.setDateOfBirth(employeeDto.getDateOfBirth());
		employeeEntity.setDepartment(departmentEntity);

		Employee EmployeeStore = employeeRepository.save(employeeEntity);

		EmployeeDto returnValue = new EmployeeDto();
		BeanUtils.copyProperties(EmployeeStore, returnValue);

		return returnValue;

	}

	@Override
	public EmployeeDto update(Long employeeId, EmployeeDto employeeDto) {
		EmployeeDto returnValue = new EmployeeDto();

		Employee employeeEntity = employeeRepository.findById(employeeId).get();
		Department departmentEntity = departmentRepository.findById(employeeDto.getDepartmentId()).get();

		employeeEntity.setName(employeeDto.getName());
		employeeEntity.setDateOfBirth(employeeDto.getDateOfBirth());
		employeeEntity.setDepartment(departmentEntity);

		Employee updateEmployee = employeeRepository.save(employeeEntity);

		BeanUtils.copyProperties(updateEmployee, returnValue);

		return returnValue;
	}

	@Override
	public void delete(Long employeeId) {
		Employee employee = employeeRepository.findById(employeeId).get();

		employeeRepository.delete(employee);
	}

	@Override
	public List<EmployeeDto> fetchEmployeesByDepartmentId(Long departmentId, int page, int limit) {
		List<EmployeeDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Department departmentEntity = departmentRepository.findById(departmentId).get();
		Page<Employee> EmployeePage = employeeRepository.findByDepartmentId(departmentEntity.getId(), pageableRequest);
		List<Employee> Employees = EmployeePage.getContent();

		for (Employee Employee : Employees) {
			EmployeeDto EmployeeDto = new EmployeeDto();
			BeanUtils.copyProperties(Employee, EmployeeDto);
			returnValue.add(EmployeeDto);
		}

		return returnValue;
	}

	@Override
	public EmployeeDto fetchById(Long employeeId) {
		EmployeeDto returnValue = new EmployeeDto();

		Employee employeeEntity = employeeRepository.findById(employeeId).get();

		if (employeeEntity == null) {
			throw new RuntimeErrorException(null, "ID is not found");
		}
		BeanUtils.copyProperties(employeeEntity, returnValue);

		return returnValue;
	}

	@Override
	public List<EmployeeDto> fetchAll(int page, int limit) {
		List<EmployeeDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Page<Employee> employeePage = employeeRepository.findAll(pageableRequest);
		List<Employee> employees = employeePage.getContent();

		for (Employee employee : employees) {
			EmployeeDto employeeDto = new EmployeeDto();

			BeanUtils.copyProperties(employee, employeeDto);
			returnValue.add(employeeDto);
		}

		return returnValue;
	}

}
