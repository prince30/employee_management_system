package employee.management.system.rest.api.util;

import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
public class UniqueCode {

	public String generateCode(int length) {
		return UUID.randomUUID().toString().substring(0,length).toUpperCase();
	}
}
