package employee.management.system.rest.api.controller;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import employee.management.system.rest.api.dto.EmployeeDto;
import employee.management.system.rest.api.request.EmployeeRequestData;
import employee.management.system.rest.api.response.EmployeeResponse;
import employee.management.system.rest.api.response.OperationStatusModel;
import employee.management.system.rest.api.response.RequestOperationStatus;
import employee.management.system.rest.api.service.EmployeeService;

@RestController
@RequestMapping("employees") // // http://localhost:9000/employee-management-system/api/v1/employees
public class EmployeeController {

	private EmployeeService employeeService;

	public EmployeeController(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<EmployeeResponse> store(@Valid @RequestBody EmployeeRequestData employeeRequestData)
			throws Exception {

		EmployeeResponse returnValue = new EmployeeResponse();

		EmployeeDto employeeDto = new EmployeeDto();

		BeanUtils.copyProperties(employeeRequestData, employeeDto);
		EmployeeDto createdEmployee = employeeService.create(employeeDto);

		BeanUtils.copyProperties(createdEmployee, returnValue);

		return new ResponseEntity<EmployeeResponse>(returnValue, HttpStatus.CREATED);
	}

	@PutMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<EmployeeResponse> update(@Valid @PathVariable Long id,
			@RequestBody EmployeeRequestData EmployeeRequestData) throws Exception {

		EmployeeResponse returnValue = new EmployeeResponse();

		EmployeeDto employeeDto = new EmployeeDto();

		BeanUtils.copyProperties(EmployeeRequestData, employeeDto);

		EmployeeDto updateEmployee = employeeService.update(id, employeeDto);

		BeanUtils.copyProperties(updateEmployee, returnValue);

		return new ResponseEntity<EmployeeResponse>(returnValue, HttpStatus.OK);
	}

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<EmployeeResponse> fetchById(@PathVariable Long id) throws Exception {

		EmployeeResponse returnValue = new EmployeeResponse();

		EmployeeDto employeeDto = employeeService.fetchById(id);

		BeanUtils.copyProperties(employeeDto, returnValue);

		return new ResponseEntity<EmployeeResponse>(returnValue, HttpStatus.FOUND);
	}

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<EmployeeResponse>> fetchAll(
			@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "limit", defaultValue = "5") int limit) {

		List<EmployeeResponse> returnValue = new ArrayList<>();

		List<EmployeeDto> employees = employeeService.fetchAll(page, limit);

		for (EmployeeDto employeeDto : employees) {
			EmployeeResponse employeeResponseModel = new EmployeeResponse();
			BeanUtils.copyProperties(employeeDto, employeeResponseModel);
			returnValue.add(employeeResponseModel);
		}

		return new ResponseEntity<List<EmployeeResponse>>(returnValue, HttpStatus.FOUND);
	}

	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<OperationStatusModel> delete(@PathVariable Long id) throws Exception {

		OperationStatusModel returnValue = new OperationStatusModel();
		returnValue.setOperationName(RequestOperationName.DELETE.name());
		employeeService.delete(id);
		returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());

		return new ResponseEntity<OperationStatusModel>(returnValue, HttpStatus.OK);
	}
}
