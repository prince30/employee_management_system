package employee.management.system.rest.api.response;

public class DepartmentResponse {

	private String name;
	private Boolean active;

	// private List<EmployeeRequestData> employees;
	// public List<TaskResponse> tasks = new ArrayList();
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
