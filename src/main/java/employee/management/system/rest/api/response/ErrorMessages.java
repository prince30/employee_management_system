package employee.management.system.rest.api.response;

public enum ErrorMessages {
	MISSING_REQUIRED_FIELD("Missing required field."),
	COULD_NOT_CREATE_USER_PROFILE("Could not create project"),
	COULD_NOT_UPDATE_USER_PROFILE("Could not update project"),
	COULD_NOT_DELETE_USER_PROFILE("Could not delete project"), 
	NO_RECORD_FOUND("No record found for provided id"),
	RECORD_ALREADY_EXISTS("Record already exists"),
	INTERNAL_SERVER_ERROR("Something went wrong. Please repeat this operation later.");

	private String errorMessage;

	ErrorMessages(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
