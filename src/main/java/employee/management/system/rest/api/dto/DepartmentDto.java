package employee.management.system.rest.api.dto;

import java.util.List;

public class DepartmentDto {

	private String name;
	private Boolean active;
	private List<EmployeeDto> employee;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<EmployeeDto> getEmployee() {
		return employee;
	}

	public void setEmployee(List<EmployeeDto> employee) {
		this.employee = employee;
	}

}
