package employee.management.system.rest.api.service;

import java.util.List;
import employee.management.system.rest.api.dto.DepartmentDto;

public interface DepartmentService {

	DepartmentDto create(DepartmentDto departmentDto);

	DepartmentDto fetchById(Long departmentId);

	DepartmentDto update(Long departmentId, DepartmentDto departmentDto);

	void delete(Long departmentId);

	List<DepartmentDto> fetchAll(int page, int limit);

}
